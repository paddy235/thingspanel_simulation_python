#!/usr/bin/python3
"""_summary_

    Returns:
        _type_: _description_
"""
# -*- coding:utf-8 -*-
#
# File Name:
# @Author:  paddy
# @Email:   3988263@qq.com
# @Time:    ${DATE} ${TIME}
# Project: ${PROJECT_NAME}
# ---------------------说明--------------------------
#
# ---------------------------------------------------
import os
import json


class Tools:
    """常用工具
    """

    root_path = os.path.dirname(
                os.path.dirname(os.path.abspath(__file__))
                )

    def writejson(self, filename, listobj):
        """保存python对象为json文件
        默认路径在/conf/data/

        Args:
            filename (str): _description_
            python_obj (list): _description_
        """
        _file = self.root_path+'/conf/data/'+filename
        with open(str(_file), 'w', encoding='utf-8') as fp_file:
            json.dump(listobj, fp_file, indent=4)

    def print_ver(self) -> None: ...
