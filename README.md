## 1. 关于本项目

为了使用[ThingsPanel](http://thingspanel.io/),模拟数据仿真测试，也可以改造为压力测试使用

当前还有很多功能未开发，待完善中.

## 2. 使用方法

前提需求：python3

### 2.1 安装[ThingsPanel](http://thingspanel.io/zh-Hans/docs/category/%E7%B3%BB%E7%BB%9F%E5%AE%89%E8%A3%85)

创建租户用户和密码，并配置到本项目的配置文件[config.ini](conf/config.ini)中

### 2.2. 运行本程序

#### 2.2.1 配置[config.ini](conf/config.ini)

参见详细说明[docs/config.md](docs/config.md)

### 2.2.2 安装程序依赖库

`pip freeze > requirements.txt`
`pip install -r requirements.txt `

### 2.2.3 运行程序

[test_services.py](test_services.py) 是测试用例，可以生成[report_test.html](report_test.html)报告

[thingspanel_simulation.py](thingspanel_simulation.py) 是运行主程序

### 2.2.4 停止运行

运行窗口中按 `CTRL+C` 终止运行


## 3. 其他说明

[docs](docs)下有配置文件等说明

`bin`目录下有两个小脚本，用来测试api和返回结果

测试使用的 http://tungwaiyip.info/software/HTMLTestRunner.html

[HTMLTestReportCN.py](HTMLTestReportCN.py) 可以复制到`sys.path`下长期使用


### 3.1 测试效果

```
设备id: '053deb2f-e69a-4cfc-584a-23148b85c9c8',发送: {'二氧化碳(CO2': '152.2268', '甲烷(CH4)': '85.5303', '氧化亚氮(N2O)': '112.4436', '电池电量': '13.4274'}
.
Time Elapsed: 0:00:00.196334
Call 'connect_mqtt'
设备id: '1fcb3295-22fb-5454-5f66-a4de1160952b',发送: {'二氧化碳(CO2': '241.3795', '甲烷(CH4)': '68.9240', '氧化亚氮(N2O)': '158.7078', '电池电量': '14.8405'}
Call 'connect_mqtt'
设备id: 'cefc712a-fd7e-b417-e5e5-658a4e81873a',发送: {'二氧化碳(CO2': '193.3639', '甲烷(CH4)': '51.0097', '氧化亚氮(N2O)': '121.1389', '电池电量': '85.7233'}
设备id: '717fd669-f1ee-bedf-41f6-838712e01d12',发送: {'二氧化碳(CO2': '206.7909', '甲烷(CH4)': '73.2421', '氧化亚氮(N2O)': '188.5137', '电池电量': '11.0637'}
设备id: '9f772ff1-e933-e1ab-8f88-8cbd41ee3676',发送: {'二氧化碳(CO2': '221.6497', '甲烷(CH4)': '73.7895', '氧化亚氮(N2O)': '113.6067', '电池电量': '69.8545'}
设备id: '8626074a-9d74-5913-7301-c75eed30acb9',发送: {'二氧化碳(CO2': '178.8170', '甲烷(CH4)': '53.6320', '氧化亚氮(N2O)': '157.1029', '电池电量': '90.1932'}
设备id: 'e855deca-8c3a-a5e7-79ff-e29a6dc6707b',发送: {'二氧化碳(CO2': '182.4323', '甲烷(CH4)': '75.2532', '氧化亚氮(N2O)': '177.2785', '电池电量': '64.7693'}
^CCall 'disconnect'
Call 'disconnect'
Call 'disconnect'
Call 'disconnect'
Call 'disconnect'
Call 'disconnect'
接收终止命令，键盘信号 (ID: 2)         停止和清理子线程..
Call 'disconnect'
Call 'disconnect'
Call 'disconnect'
Call 'disconnect'
Call 'disconnect'
Call 'disconnect'
关闭线程: 'MqttManager-555c370d-7979-f983-2d1a-6ed3f355f98e'
关闭线程: 'MqttManager-9dcb141b-c14c-85e9-2723-8452b973cfd4'
关闭线程: 'MqttManager-d229ed99-5151-2496-02ef-0975f2b809c4'
关闭线程: 'MqttManager-053deb2f-e69a-4cfc-584a-23148b85c9c8'
关闭线程: 'MqttManager-a24aded8-9ade-832b-0e7f-be575002ac6a'
关闭线程: 'MqttManager-1fcb3295-22fb-5454-5f66-a4de1160952b'
关闭线程: 'MqttManager-cefc712a-fd7e-b417-e5e5-658a4e81873a'
关闭线程: 'MqttManager-2123571c-48b2-2edb-8382-cb9d9c1ed0be'
关闭线程: 'MqttManager-717fd669-f1ee-bedf-41f6-838712e01d12'
关闭线程: 'MqttManager-9f772ff1-e933-e1ab-8f88-8cbd41ee3676'
关闭线程: 'MqttManager-8626074a-9d74-5913-7301-c75eed30acb9'
关闭线程: 'MqttManager-e855deca-8c3a-a5e7-79ff-e29a6dc6707b'
```
