#!/usr/bin/python3
"""_summary_

    Returns:
        _type_: _description_
"""
# -*- coding:utf-8 -*-
#
# File Name:
# @Author:  paddy
# @Email:   3988263@qq.com
# @Time:    ${DATE} ${TIME}
# Project: ${PROJECT_NAME}
# ---------------------说明--------------------------
#
# ---------------------------------------------------

import logging
from service.myconf import MyConfigParser
from service.client import WebApi
from service.manager import Business
from service.generator_device import Manager
# from service.mqtt_client import DeviceAttrib, MqttManager

logger = logging.getLogger(__name__)
logger.setLevel(level=logging.INFO)

console = logging.StreamHandler()
console.setLevel(logging.INFO)
logger.addHandler(console)

if __name__ == "__main__":
    print('测试开始')
    myconf = MyConfigParser()
    url = myconf.get_config('Server', 'URL')
    username = myconf.get_config('Server', 'USERNAME')
    password = myconf.get_config('Server', 'PASSWORD')
    clear_data = str(
        myconf.get_config('Simulation', 'CLEARDATA')
        ).lower()
    webapi = WebApi(url, username, password)
    webapi.get_access_token()
    busines_cls = Business(webapi)
    # 如果需要清理数据
    if clear_data == 'true':
        logger.info("先清理")
        busines_cls.clear_all_data()  # 先清理
        logger.info("再创建")
        busines_cls.cerate_entrying()  # 再创建
    logger.info("读取在线数据")
    # 单独跑数据 == MqttManager
    # busines_cls.load_entrying()
    # device_id = busines_cls.get_device_array[0]['device_id']
    # deviceattrib = DeviceAttrib()
    # deviceattrib.mqtt_host = myconf.get_config('Mqtt', 'MQTT_HOST')
    # deviceattrib.mqtt_port = myconf.get_config('Mqtt', 'MQTT_PORT')
    # deviceattrib.device_name = '环境采集器'
    # deviceattrib.device_id = device_id
    # deviceattrib.token = device_id
    # deviceattrib.topic = myconf.get_config('Mqtt', 'TOPIC')
    # deviceattrib.client_id = myconf.get_config('Mqtt', 'CLIENT')
    # mqtt_s = MqttManager(webapi, deviceattrib)
    # mqtt_s.start()
    # mqtt_s.disconnect()

    # 批量跑数据 == Manager
    generator_dev = Manager(webapi, myconf)
    generator_dev.load_entrying()  # 读取在线数据
    generator_dev.start()
