#!/usr/bin/python3
"""_summary_
"""
# -*- coding:utf-8 -*-
#
# File Name:
# @Author:  paddy
# @Email:   3988263@qq.com
# @Time:    ${DATE} ${TIME}
# Project: ${PROJECT_NAME}
# ---------------------说明--------------------------
#
# ---------------------------------------------------

import unittest
import logging
import HTMLTestReportCN
from service.myconf import MyConfigParser
from service.client import WebApi
from service.manager import Business
from service.generator_device import Manager
from service.mqtt_client import DeviceAttrib, MqttManager


# 继承unittest.TestCase
class MyTest(unittest.TestCase):
    """_summary_

    Args:
        unittest (_type_): _description_
    """
    logger = logging.getLogger(__name__)
    logger.setLevel(level=logging.INFO)
    console = logging.StreamHandler()
    console.setLevel(logging.INFO)
    logger.addHandler(console)
    # 读取业务返回结果
    business = None
    busines_cls = None

    def setUp(self):
        """每次执行一遍
        """
        # 每个测试用例执行之前做操作
        print('测试setUp开始')

    @classmethod
    def setUpClass(cls):
        """初始化执行一次
        """
        print('测试开始')
        cls.myconf = MyConfigParser()
        url = cls.myconf.get_config('Server', 'URL')
        username = cls.myconf.get_config('Server', 'USERNAME')
        password = cls.myconf.get_config('Server', 'PASSWORD')
        cls.clear_data = str(
            cls.myconf.get_config('Simulation', 'CLEARDATA')
            ).lower()
        cls.webapi = WebApi(url, username, password)
        cls.busines_cls = Business(cls.webapi)

    def test_login(self):
        """测试登陆是否成功,并获得token
        """
        self.webapi.get_access_token()
        self.assertIsNotNone(self.webapi.access_token, 'access_token不存在')

    def test_product(self):
        """ 测试产品添加和删除
        """
        data = {
            "name": "tp-温湿度",
            "serial_number": "TP-WSD-001",
            "protocol_type": "mqtt",
            "auth_type": "1",
            "plugin": "{}",
            "describe": "产品测试"
        }
        status_code, message = self.webapi.api_tp_product_add(data)
        self.assertTrue(status_code == 200, 'status_code is error')
        data_id = message['data']['id']
        self.logger.info(message['message'])
        # 删除产品
        data2 = {
            "id": data_id
        }
        status_code, message = self.webapi.api_tp_product_delete(data2)
        self.assertTrue(status_code == 200, 'status_code is error')
        self.logger.info(message['message'])

    def test_busines(self):
        """测试添加业务
        """
        # 如果需要清理数据
        if self.clear_data == 'true':
            self.logger.info("先清理")
            self.busines_cls.clear_all_data()  # 先清理
            self.logger.info("再创建")
            self.busines_cls.cerate_entrying()  # 再创建
        self.logger.info("读取在线数据")
        self.busines_cls.load_entrying()  # 读取在线数据
        self.assertIsNotNone(self.busines_cls.get_business['asset_id'])
        self.assertTrue(len(self.busines_cls.get_device_array) > 0)
        self.logger.info("测试添加业务-测试完毕")

    def test_mqtt_client(self):
        """测试单一设备模拟数据
        """
        # 初始化设备参数
        self.logger.info("测试单一设备模拟数据-测试开始")
        device_id = self.busines_cls.get_device_array[0]['device_id']
        deviceattrib = DeviceAttrib()
        deviceattrib.mqtt_host = self.myconf.get_config('Mqtt', 'MQTT_HOST')
        deviceattrib.mqtt_port = self.myconf.get_config('Mqtt', 'MQTT_PORT')
        deviceattrib.device_name = '环境采集器'
        deviceattrib.device_id = device_id
        deviceattrib.token = device_id
        deviceattrib.username = device_id
        deviceattrib.client_id = device_id

        mqtt_s = MqttManager(self.webapi, deviceattrib)
        mqtt_s.start()
        mqtt_s.disconnect()

    def test_generator_device(self):
        """测试所有设备随机模拟数据
        """
        self.logger.info("测试所有设备随机模拟数据-测试开始")
        generator_dev = Manager(self.webapi, self.myconf)
        generator_dev.load_entrying()
        generator_dev.start()
        self.logger.info("测试所有设备随机模拟数据-测试完毕")


# 添加Suite
def suite_all():
    """测试所有case用例

    Returns:
        _type_: _description_
    """
    # 定义一个单元测试容器
    suite_test = unittest.TestSuite()
    # 将测试用例加入到容器
    suite_test.addTest(MyTest("test_login"))
    suite_test.addTest(MyTest("test_busines"))
    suite_test.addTest(MyTest("test_generator_device"))
    return suite_test


if __name__ == '__main__':
    with open('report_test.html', 'wb') as file:
        # 生成报告的Title,描述
        runner = HTMLTestReportCN.HTMLTestRunner(
            stream=file,
            title='自动化测试报告',
            # description='详细测试用例结果',
            tester='Findyou'
        )
        runner.run(suite_all())
