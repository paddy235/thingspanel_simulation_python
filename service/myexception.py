#!/usr/bin/python3
"""_summary_

    Returns:
        _type_: _description_
"""
# -*- coding:utf-8 -*-
#
# File Name:
# @Author:  paddy
# @Email:   3988263@qq.com
# @Time:    ${DATE} ${TIME}
# Project: ${PROJECT_NAME}
# ---------------------说明--------------------------
#
# ---------------------------------------------------
import logging
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
console = logging.StreamHandler()
console.setLevel(logging.INFO)
logger.addHandler(console)


def error_management(func):
    """_summary_

    Args:
        func (_type_): _description_
    """
    def func_wrapper(*args, **kwargs):
        try:
            logger.info("Call %r", func.__name__)
            return func(*args, **kwargs)
        except KeyError:
            logger.exception("Invalid value received")
            return func(*args, **kwargs)

    return func_wrapper
