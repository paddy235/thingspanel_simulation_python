#!/usr/bin/python3
"""_summary_

    Raises:
        WebApiError: _description_

    Returns:
        _type_: _description_
    """
# -*- coding:utf-8 -*-
#
# File Name:
# @Author:  paddy
# @Email:   3988263@qq.com
# @Time:    ${DATE} ${TIME}
# Project: ${PROJECT_NAME}
# ---------------------说明--------------------------
#
# ---------------------------------------------------
from configparser import ConfigParser
import os


class MyConfigParser:
    """_summary_

    Returns:
        _type_: _description_
    """
    config_dic = {}

    @classmethod
    def get_config(cls, sector, item):
        """_summary_

        Args:
            sector (_type_): _description_
            item (_type_): _description_

        Returns:
            _type_: _description_
        """
        configfile = '/conf/config.ini'
        try:
            if cls.config_dic[sector][item]:
                return cls.config_dic[sector][item]
            return None
        except KeyError:
            try:
                config = ConfigParser()
                config_path = os.path.dirname(
                    os.path.dirname(os.path.abspath(__file__))
                    )
                config_path = config_path + configfile
                config.read(config_path)
                cls.config_dic = config
                if cls.config_dic[sector][item]:
                    return cls.config_dic[sector][item]
                return None
            except KeyError as aerror:
                print("KeyError=", aerror)
                return None

    def print(self):
        """_summary_
        """
        print(__name__)
