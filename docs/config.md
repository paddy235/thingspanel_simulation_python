# config.ini的配置内容

```
[default]
## 随时可以留作日至使用
logfile=logs/log.txt   

# web服务器配置
[Server]
URL=http://192.168.19.23:8080

# 安装后创建的租户账户和密码配置到这里
USERNAME=paddy@qq.com
PASSWORD=DdLrGaKN5uRp0K05D7JY
# 程序自动获得，这里无需配置
AUTH_TOKEN=

# mqtt服务配置说明
[Mqtt]
MQTT_HOST=192.168.19.23
MQTT_PORT=1883
CLIENT=paddydemotest
TOPIC=device/attributes

# 仿真配置
[Simulation]

# 是否清除原始数据，如果是True,则删除原始数据，重新创建；
# 否则，保留原始数据，继续更新设备的仿真数据
CLEARDATA=false
```