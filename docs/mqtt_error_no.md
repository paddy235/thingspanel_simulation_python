## MQTT错误码

错误码 说明（标粗表示是可能的client错误）

0 成功

```
---------------------MQTT协议规定的连接错误-------------------------
1 连接拒绝：协议版本不接受
2 连接拒绝：标识符被拒绝
3 连接拒绝：服务不可用
4 连接拒绝：错误的用户名或密码
5 连接拒绝：没有通过认证
---------------------PAHO定义的错误-------------------------
-1 Failure，一般故障，如socket发送失败等
-2 Persistence error，留存错误
-3 Disconnected，断开连接
-4 Maximum in-flight messages amount reached，超过客户端数据并发数
-5 Invalid UTF8 string，检测到无效的UTF-8字符串
-6 Invalid (NULL) parameter，检测到参数空指针
-7 Topic containing NULL characters has been truncated，Topic字符串包含\0字符串，而非完整的Topic
-8 Bad structure，错误的协议结构体
-9 Invalid QoS value，错误的QOS，可接受的为(0,1,2)
-10 Too many pending commands，消息ID大于65535
-11 Operation discarded before completion，完成前丢弃的操作，一般为发送过程中socket断开
-12 No more messages can be buffered，无法缓冲更多的消息
-13 SSL is not supported，SSL不支持
-14 Invalid protocole scheme，错误的协议类型，应该是tcp:// or ssl://
-15 Options for wrong MQTT version，错误的mqtt版本
-16 Client created for another version of MQTT，mqtt客户端是用另外的MQTT版本创建的
```