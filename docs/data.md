## conf/data目录文件说明

* [asset_module.json](../conf/data/asset_module.json) 创建业务项目名称后，子目录结构数据

程序会根据子目录中是否有"tier"来判断最后一级，并在这个子目录下开始创建设备

* [business.json](../conf/data/business.json) 创建业务项目名称

* [device_module.json](../conf/data/device_module.json) 统一创建设备的模板

* [device_attrib_module.json](../conf/data/device_attrib_module.json) 统一设备的属性和最大值最小值配置(预留未使用)

* [device_id.json](../conf/data/device_id.json) 创建新设备后，将创建的设备id临时保存在本地一份, 为了未来本地加载测试用 (保留未使用)

* [device_asset_id.json](../conf/data/device_asset_id.json) 创建新目录后，将创建的目录id临时保存在本地一份, 为了未来本地加载测试用 (保留未使用)

* [customer.json](../conf/data/customer.json) 测试用例数据 (保留未使用)

