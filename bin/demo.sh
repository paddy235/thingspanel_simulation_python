#!/bin/bash
URL="http://192.168.19.23:8080"
USERNAME="paddy@qq.com"
PASSWORD='DdLrGaKN5uRp0K05D7JY'
AUTH_TOKEN=""
CURLCMD="curl -s -X POST"
PRODUCT_ID=""
TP_BATCH_ID=""


UserAgent='User-Agent: Chrome/95.0.4638.69 Safari/537.36 Edg/95.0.1020.53'
ContentType='Content-Type: application/json'

item=0
bold=$(tput bold)
underline=$(tput sgr 0 1)
reset=$(tput sgr0)

red=$(tput setaf 1)
green=$(tput setaf 76)
white=$(tput setaf 7)
tan=$(tput setaf 202)
blue=$(tput setaf 25)

underline() { printf "${underline}${bold}%s${reset}\n" "$@"
}
h1() { printf "\n${underline}${bold}${blue}%s${reset}\n" "$@"
}
h2() { printf "\n${underline}${bold}${white}%s${reset}\n" "$@"
}
debug() { printf "${white}%s${reset}\n" "$@"
}
info() { printf "${white}➜ %s${reset}\n" "$@"
}
success() { printf "${green}✔ %s${reset}\n" "$@"
}
error() { printf "${red}✖ %s${reset}\n" "$@"
}
warn() { printf "${tan}➜ %s${reset}\n" "$@"
}
bold() { printf "${bold}%s${reset}\n" "$@"
}
note() { printf "\n${underline}${bold}${blue}Note:${reset} ${blue}%s${reset}\n" "$@"
}



function result_parse(){

result=$1

	if [ -n "${result}" ]; then	
		result_code=$(echo ${result}|jq .code)
		
		message=$(echo $result|jq .message)
		
		if [ ${result_code} -ne 200 ]; then
			echo "ERROR: code=${result_code}, message=${message}"
			exit 1;
	    	else
	    		echo $result|jq .data

	    		echo "操作完毕"
	    		
	    	
	    	fi
	else
		echo "ERROR: result=${result}"
		exit 1;
	fi

}

function tp_login(){

	h2 "[Step $item]: ${FUNCNAME} ..."; let item+=1

	result=$(${CURLCMD} "${URL}/api/auth/login" \
	--header "${UserAgent}" \
	--header "${ContentType}" \
	--data-raw "{	
	    \"email\": \"${USERNAME}\",
	    \"password\": \"${PASSWORD}\"
	}")

	AUTH_TOKEN=$(echo ${result}|jq .data.access_token)
	AUTH_TOKEN=${AUTH_TOKEN//\"/}

}

function production_add(){

	h2 "[Step $item]: ${FUNCNAME} ..."; let item+=1

	
	
	result=$(${CURLCMD} "${URL}/api/tp_product/add" \
	--header "Authorization: Bearer ${AUTH_TOKEN}" \
	--header "${UserAgent}" \
	--header "${ContentType}" \
	--data-raw '{
	    "name": "tp-温湿度",
	    "serial_number": "TP-WSD-001",
	    "protocol_type": "mqtt",
	    "auth_type": "1",
	    "plugin": "{}",
	    "describe": "产品测试"
	}')
	
	result_code=$(echo $result|jq .code)
	
	message=$(echo $result|jq .message)
	
	if [ $result_code -ne 200 ]; then
        	echo "ERROR: code=${result_code}, message=${message}"
        	exit 1;
    	else
    		echo $(echo $result|jq .data)
    		PRODUCT_ID=$(echo $result|jq .data.id)
    		PRODUCT_ID=${PRODUCT_ID//\"/}
    		
    		echo "${PRODUCT_ID}产品增加完毕"
    		
    	
    	fi



}

## 添加批次
function tp_batch_add(){

	h2 "[Step $item]: ${FUNCNAME} ..."; let item+=1

	result=$(${CURLCMD} ${URL}"/api/tp_batch/add" \
	--header "Authorization: Bearer ${AUTH_TOKEN}" \
	--header "${UserAgent}" \
	--header "${ContentType}" \
	--data-raw "{

		\"batch_number\": \"BATCH009\",

		\"product_id\": \"${PRODUCT_ID}\",

		\"device_number\": 5,

		\"generate_flag\": \"\",

		\"describle\": \"\",

		\"access_address\":\"123456\"

	}")

	

	if [ "${result}" != "" ]; then	
		result_code=$(echo ${result}|jq .code)
		
		message=$(echo $result|jq .message)
		
		if [ ${result_code} -ne 200 ]; then
			echo "ERROR: code=${result_code}, message=${message}"
			exit 1;
	    	else
	    		echo $(echo $result|jq .data)
	    		TP_BATCH_ID=$(echo $result|jq .data.id)
	    		TP_BATCH_ID=${TP_BATCH_ID//\"/}
	    		echo "批次增加完毕"
	    		
	    	
	    	fi
	else
		echo "ERROR: ${FUNCNAME} result=${result}"
		exit 1;
	fi

}

## 批次生成
function tp_batch_generate(){

	h2 "[Step $item]: ${FUNCNAME} ..."; let item+=1

	result=$(${CURLCMD} ${URL}"/api/tp_batch/generate" \
	--header "Authorization: Bearer ${AUTH_TOKEN}" \
	--header "${UserAgent}" \
	--header "${ContentType}" \
	--data-raw "{
	\"id\": \"${TP_BATCH_ID}\"
	}")

	

	result_parse "${result}"


}

## 业务添加-T
function api_business_add(){

	h2 "[Step $item]: ${FUNCNAME} ..."; let item+=1
	
	result=$(${CURLCMD} ${URL}"/api/business/add" \
	--header "Authorization: Bearer ${AUTH_TOKEN}" \
	--header "${UserAgent}" \
	--header "${ContentType}" \
	--data-raw "{
	\"name\": \"业务添加01\"
	}")

	

	result_parse "${result}"
}

## 业务删除-T
function api_business_delete(){

	local business_id="$1"
	
	h2 "[Step $item]: ${FUNCNAME} ..."; let item+=1
	
	result=$(${CURLCMD} ${URL}"/api/business/delete" \
	--header "Authorization: Bearer ${AUTH_TOKEN}" \
	--header "${UserAgent}" \
	--header "${ContentType}" \
		--data-raw "{
    		\"id\": \"${business_id}\"
	}")

	result_parse "${result}"
}

function api_device_list_tree(){

current_page=${1:-'1'}
per_page=${2:-'20'}
business_id=${3:-"2b3197de-fec1-f954-2b91-7627987cbea0"}

h2 "[Step $item]: ${FUNCNAME} ..."; let item+=1

	info "business_id=${business_id},current_page=${current_page},per_page=${per_page}"

	result=$(${CURLCMD} ${URL}'/api/device/list/tree' \
	--header "Authorization: Bearer ${AUTH_TOKEN}" \
	--header "${UserAgent}" \
	--header "${ContentType}" \
	--data-raw "{
	    \"current_page\": ${current_page},
	    \"per_page\": ${per_page},
		\"device_id\":  \"\",
		\"business_id\":  \"${business_id}\",
		\"asset_id\":  \"\",
		\"device_type\":  \"\",
		\"token\":  \"\"
	}")	
	result_parse "${result}"

}
function device_add(){

	h2 "[Step $item]: ${FUNCNAME} ..."; let item+=1

	result=$(${CURLCMD} ${URL}'/api/device/add_only' \
	--header "Authorization: Bearer ${AUTH_TOKEN}" \
	--header "${UserAgent}" \
	--header "${ContentType}" \
	--data-raw '{
	    "asset_id": "$1",
	    "token": "111",
	    "type": "1",
	    "name": "demo01",
	    "extension": "",
	    "protocol": "mqtt",
	    "d_id": "",
	    "location":""

	}')

	result_parse "${result}"

}
function asset_list_d(){


	h2 "[Step $item]: ${FUNCNAME} ..."; let item+=1


	local business_id="$1"

	result=$(${CURLCMD} ${URL}"/api/asset/list/d" \
	--header "Authorization: Bearer ${AUTH_TOKEN}" \
	--header "${UserAgent}" \
	--header "${ContentType}" \
	--data-raw "{
    		\"business_id\": \"${business_id}\"
	}")

	result_parse "${result}"

}

function api_business_index(){


	h2 "[Step $item]: ${FUNCNAME} ..."; let item+=1

	result=$(${CURLCMD} ${URL}"/api/business/index" \
	--header "Authorization: Bearer ${AUTH_TOKEN}" \
	--header "${UserAgent}" \
	--header "${ContentType}" \
	--data-raw "{
    		\"name\": \"工控协议测试项目\",
    		\"limit\": 1,
    		\"page\": 1
    		
	}")
	
	BUSINESS_ID=$(echo ${result}|jq .data.data[0].id)
	BUSINESS_ID=${BUSINESS_ID//\"/}
	echo ${result}|jq .data.data[0]

}

function deleteAsset(){

	h2 "[Step $item]: ${FUNCNAME} ..."; let item+=1

	local id=$1

	result=$(${CURLCMD} ${URL}"/api/asset/delete" \
	--header "Authorization: Bearer ${AUTH_TOKEN}" \
	--header "${UserAgent}" \
	--header "${ContentType}" \
	--data-raw "{
    		\"id\": \"${id}\"
	}")

	
	result_parse "${result}"

}

function asset_add_only(){

	h2 "[Step $item]: ${FUNCNAME} ..."; let item+=1


	local Name=$1
	local Tier=$2
	local ParentID=$3
	local BusinessID=$4


	result=$(${CURLCMD} ${URL}"/api/asset/add_only" \
	--header "Authorization: Bearer ${AUTH_TOKEN}" \
	--header "${UserAgent}" \
	--header "${ContentType}" \
	--data-raw "{
    		\"name\": \"${Name}\",
    		\"tier\": \"${Tier}\",
    		\"parent_id\": \"${ParentID}\",
    		\"business_id\": \"${BusinessID}\"
	}")

	
	result_parse "${result}"

}

## 增加分组的api，必须条件中的  ParentID 从哪个api获得？
function api_asset_list_d(){

	h2 "[Step $item]: ${FUNCNAME} ..."; let item+=1

	local BusinessID=$1


	result=$(${CURLCMD} ${URL}"/api/asset/list/d" \
	--header "Authorization: Bearer ${AUTH_TOKEN}" \
	--header "${UserAgent}" \
	--header "${ContentType}" \
	--data-raw "{
    		\"business_id\": \"${BusinessID}\"
	}")

	
	result_parse "${result}"

}

## 数据条件查询
function api_kv_index(){

	h2 "[Step $item]: ${FUNCNAME} ..."; let item+=1

	result=$(${CURLCMD} ${URL}"/api/kv/index" \
	--header "Authorization: Bearer ${AUTH_TOKEN}" \
	--header "${UserAgent}" \
	--header "${ContentType}" \
	--data-raw "{
	  \"business_id\": \"\",
	  \"limit\": 10,
	  \"page\": 1,
	  \"entity_id\": \"\",
	  \"type\": 4,
	  \"start_time\": \"2023-07-6 00:00:00\",
	  \"token\": \"\",
	  \"end_time\": \"2023-07-17 23:59:59\",
	  \"key\": \"\"
	}")

	
	result_parse "${result}"

}

## 数据条件查询
function api_device_list(){

	h2 "[Step $item]: ${FUNCNAME} ..."; let item+=1

	result=$(${CURLCMD} ${URL}"/api/device/list" \
	--header "Authorization: Bearer ${AUTH_TOKEN}" \
	--header "${UserAgent}" \
	--header "${ContentType}" \
	--data-raw "{
		    \"current_page\": 1,
		    \"per_page\": 10,
		    \"device_id\": \"\",
		    \"business_id\": \"\",
		    \"asset_id\": \"\",
		    \"device_type\": \"\",
		    \"token\": \"\"
	}")

	
	result_parse "${result}"

}

## 数据条件查询
function api_asset_list_d(){

	h2 "[Step $item]: ${FUNCNAME} ..."; let item+=1

	result=$(${CURLCMD} ${URL}"/api/asset/list/d" \
	--header "Authorization: Bearer ${AUTH_TOKEN}" \
	--header "${UserAgent}" \
	--header "${ContentType}" \
	--data-raw "{
		    \"business_id\": \"${BUSINESS_ID}\"

	}")

	
	result_parse "${result}"

}

function main(){

	h2 "[Step $item]: ${FUNCNAME} ..."; let item+=1


tp_login

if [ "" != "${AUTH_TOKEN}" ]; then

echo "AUTH_TOKEN is ${AUTH_TOKEN}"
	api_business_index
	#production_add
	#tp_batch_add
	#tp_batch_generate
	#api_business_add
	##device_add "f6a6f84f-8cd0-bf80-79f0-1bbffd1bb5af"
	
	##deleteAsset "ad5c5975-88a0-7fea-ac86-b25a6655afaa"
	##asset_list_d "6f8f8947-af3f-c69a-fd6f-40f0053c08e5"
	##asset_add_only "采集区1" "5" "55a54e44-6bf0-06e8-2d26-b465d368ea0a" "6f8f8947-af3f-c69a-fd6f-40f0053c08e5"
	##deleteAsset "7313a7bd-c013-5a34-c272-dda2b9227d53"
	##api_business_delete "6f8f8947-af3f-c69a-fd6f-40f0053c08e5"
	##api_asset_list_d "a2b22c9b-473a-c65a-44f1-b08fcc8827a9"
	## api_device_list_tree 1 20 ${BUSINESS_ID}
	#api_kv_index
	#api_asset_list_d
	api_device_list

else
	echo "error:TOKEN is ${TOKEN}".

fi
}




main




























